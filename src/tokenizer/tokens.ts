export enum TokenKeys {
	SPACE = "SPACE",
	L_BRACKET = "L_BRACKET",
	R_BRACKET = "R_BRACKET",
	IDENTIFIER = "IDENTIFIER",
	OP_PLUS = "OP_PLUS",
	NEW_LINE = "NEW_LINE",
	TAB = "TAB",
	EOF = "EOF", // This is special,
	KW_PROTECTED = "KW_PROTECTED",
	KW_OUT = "KW_OUT",
	KW_IMPORT = "KW_IMPORT",
	KW_MAP = "KW_MAP",
	KW_PUT = "KW_PUT",
	KW_ELSE = "KW_ELSE",
	KW_IF = "KW_IF",
	KW_RETURN = "KW_RETURN",
	KW_FOR = "KW_FOR",
	KW_STRING = "KW_STRING",
	KW_VAR = "KW_VAR",
	KW_NEW = "KW_NEW",
	L_BRACE = "L_BRACE",
	R_BRACE = "R_BRACE",
	KW_IN  = "KW_IN",
	KW_OPERATION = "KW_OPERATION",
	SINGLE_QUOTE = "SINGLE_QUOTE",
	DOUBLE_QUOTE = "DOUBLE_QUOTE",
	COMMA = "COMMA",
	DOT = "DOT",
	SEMI_COLON = "SEMI_COLON",
	B_SLASH = "B_SLASH",
	F_SLASH = "F_SLASH",
	STAR = "STAR",
	DASH = "DASH",
	COLON = "COLON",
	EQUALS = "EQUALS",
	AT = "AT",
	KW_CACHED = "KW_CACHED",
	NUMBER = "NUMBER"
}

export const TOKEN_MATCHERS = {
			[TokenKeys.SPACE]: / /gm,
			[TokenKeys.L_BRACKET]: /\(/,
			[TokenKeys.R_BRACKET]: /\)/,
			[TokenKeys.R_BRACE]: /\}/,
			[TokenKeys.L_BRACE]: /\{/,
			[TokenKeys.SINGLE_QUOTE]: /\'/,
			[TokenKeys.DOUBLE_QUOTE]: /"/,
			[TokenKeys.B_SLASH]: /\\/,
			[TokenKeys.AT]: /@/,
			[TokenKeys.F_SLASH]: /\//,
			[TokenKeys.STAR]: /\*/,
			[TokenKeys.COLON]: /:/,
			[TokenKeys.DASH]: /-/,
			[TokenKeys.DOT]: /\./,
			[TokenKeys.SEMI_COLON]: /;/,
			[TokenKeys.KW_PROTECTED]: /\bprotected\b/,
			[TokenKeys.KW_NEW]: /new/,
			[TokenKeys.KW_OPERATION]: /\boperation\b/,
			[TokenKeys.KW_CACHED]: /cached/,
			[TokenKeys.KW_PUT]: /put/,
			[TokenKeys.KW_MAP]: /\bMap\b/,
			[TokenKeys.KW_ELSE]: /\belse\b/,
			[TokenKeys.KW_FOR]: /\bfor\b/,
			[TokenKeys.KW_STRING]: /\bString\b/,
			[TokenKeys.KW_IN]: /\bin\b/,
			[TokenKeys.KW_VAR]: /\bvar\b/,
			[TokenKeys.EQUALS]: /\=/,
			[TokenKeys.KW_IF]: /if/,
			[TokenKeys.KW_RETURN]: /\breturn\b/,
			[TokenKeys.KW_OUT]: /\bout\b/,
			[TokenKeys.KW_IMPORT]: /\bimport\b/,
			[TokenKeys.IDENTIFIER]: /\w+/,
			[TokenKeys.OP_PLUS]: /\+/,
			[TokenKeys.NEW_LINE]: /\n/,
			[TokenKeys.TAB]: /\t/,
			[TokenKeys.COMMA]: /,/,
			[TokenKeys.NUMBER]: /[0-9]/
		};

export interface TokenLocation {
	start: number;
	end: number;
	line: number;
}

export interface Token {
	name: string;
	value: string;
	location: TokenLocation;
}

export interface Line {
	contents: string;
	number: number;
}

export const EOF_TOKEN: Token = {
	name: "EOF",
	value: "EOF",
	location: {
		start: 0,
		end: 0,
		line: 0
	}
};
