import { TokenStream } from './token-stream';
import { Token, TOKEN_MATCHERS } from './tokens';

export class Tokenizer {
	private _tokenStream: Array<Token> = [];

	process(text: string, lineNumber = 0, startIndex = 0): TokenStream {

		let currentIndex = startIndex;

		// To tokenize we split on word or whitespace boundaries.
		let rawTokens = text.split(/(\b|\s|\W)/).filter(i => i.length > 0);

		// Match tokens
		for (let i = 0; i < rawTokens.length; ++i) {
			const currentToken = rawTokens[i];

			for (const key in TOKEN_MATCHERS) {
				if (currentToken.match(TOKEN_MATCHERS[key])) {
					this._tokenStream.push({
						name: key,
						value: currentToken,
						location: {
							start: currentIndex,
							end: currentIndex + currentToken.length,
							line: lineNumber
						}
					});
					break;
				}
			}

			currentIndex += currentToken.length;
		}
		return new TokenStream(this._tokenStream);
	}
}