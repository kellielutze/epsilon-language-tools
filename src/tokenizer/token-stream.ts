import { EOF_TOKEN, Token, TokenKeys } from "./tokens";

export class TokenStream {
	private _tokenStream: Array<Token> = [];
	private _numberOfTokens = 0;
	private _index = 0;

	constructor(tokens: Array<Token>) {
		this._tokenStream = tokens;
		this._numberOfTokens = this._tokenStream.length;
	}

	nextToken() {
		if (this._index === this._numberOfTokens - 1) {
			return EOF_TOKEN;
		}
		return this._tokenStream[this._index + 1];
	}

	nextNextToken() {
		if (this._index + 1 === this._numberOfTokens - 1) {
			return EOF_TOKEN;
		}
		return this._tokenStream[this._index + 2];
	}

	nextTokenKey() {
		return TokenKeys[(this.nextToken()).name];
	}

	nextNextTokenKey() {
		return TokenKeys[(this.nextNextToken()).name];
	}

	previousToken(): Token {
		if (this._index === 0) {
			return EOF_TOKEN;
		}
		return this._tokenStream[this._index - 1];
	}

	previousTokenKey(): TokenKeys {
		return TokenKeys[this.previousToken().name];
	}

	currentToken(): Token {
		return this._tokenStream[this._index];
	}

	consumeSpecificToken(tokenKey: TokenKeys): boolean {
		if (this.currentTokenKey() === tokenKey) {
			this.consumeToken();
			return true;
		} else {
			return false;
		}
	}

	currentTokenKey(): TokenKeys {
		return TokenKeys[this._tokenStream[this._index].name];
	}

	consumeToken(): Token {
		if (this._index === this._numberOfTokens - 1) {
			return EOF_TOKEN;
		}
		return this._tokenStream[this._index++];
	}

	/**
	 * Check if the current token matches
	 *
	 * @param token The token to check
	 */
	checkToken(token: TokenKeys): boolean {
		if (this._index === this._numberOfTokens) {
			return false;
		}
		return TokenKeys[this._tokenStream[this._index].name] === token;
	}

	checkNextTwoTokens(tokens: Array<TokenKeys>): boolean {
		return tokens.includes(this.nextTokenKey())
			|| tokens.includes(this.nextNextTokenKey());
	}

	checkPreviousToken(token: TokenKeys) {
		return token === this.previousTokenKey();
	}


	checkNextTokens(tokens: Array<TokenKeys>) {
		return tokens.includes(this.nextTokenKey());
	}

	checkTokens(tokens: Array<TokenKeys>) {
		if (this._index === this._numberOfTokens - 1) {
			return false;
		}

		return tokens.includes(this.currentTokenKey());
	}

	getTokenNames() {
		return this._tokenStream.map(token => token.name);
	}
}
