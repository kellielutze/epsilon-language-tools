import { Diagnostic } from "../helpers/diagnostics";
import { TokenStream } from "../tokenizer/token-stream";
import { EOF_TOKEN, Token, TokenKeys } from "../tokenizer/tokens";
import { AbstractParser, DiagnosticType } from "./abstract.parser";



/**
 * To operate on a single static section
 */
export class EolParser extends AbstractParser {

	constructor(
		tokenStream: TokenStream,
	) {
		super(tokenStream);
	}

	parse(): Array<Diagnostic> {
		this.parseContent();

		return this._errors;
	}

	private parseContent() {
		this.traceStep("Parse content");
		while (this._tokenStream.nextToken() != EOF_TOKEN) {
			if (this._tokenStream.currentTokenKey() === TokenKeys.SPACE) {
				this._tokenStream.consumeToken();
				this.parseContent();
			} else {
				this.parseExpression();
			}
		}
	}

	private parseExpression() {
		this.logStep("Parse expression");
		if (this._tokenStream.checkTokens([TokenKeys.IDENTIFIER, TokenKeys.KW_PROTECTED])) {
			if (this._tokenStream.checkNextTwoTokens([TokenKeys.OP_PLUS])) {
				this.parseOperation();
			} else if (this._tokenStream.checkNextTokens([TokenKeys.L_BRACKET])) {
				this.parseMethod();
			} else {
				this.parseIdentifier();
			}
		} else {
			this.raiseError(
				`Unexpected token -> Expected <expression> but got ${this._tokenStream.currentToken().value}`,
				DiagnosticType.SYNTAX
			);
			this._tokenStream.consumeToken();
		}
	}

	private parseIdentifier() {
		this.traceStep("Parse identifier");
		if (this._tokenStream.checkNextTokens([TokenKeys.L_BRACKET])) {
			this.parseMethod();
		} else if (this._tokenStream.nextTokenKey() == TokenKeys.DOT) {
			this.parseAttributeAccess();
		} else {
			this._tokenStream.consumeToken();
		}
	}

	parseMethod() {
		this.logStep("Parse method");

		if (this._tokenStream.currentTokenKey() == TokenKeys.KW_PROTECTED) {
			this.parseProtected();
		} else {
			this.consumeSpecificToken(TokenKeys.IDENTIFIER);
			this.consumeSpecificToken(TokenKeys.L_BRACKET);

			if (this._tokenStream.currentTokenKey() === TokenKeys.IDENTIFIER) {
				this.parseExpression();
			}

			// Handle random whitespace
			this.consumeInvalidWhitespace(TokenKeys.R_BRACKET);

			this.consumeSpecificToken(
				TokenKeys.R_BRACKET,
				`Unexpected token -> Expected ) but got ${this._tokenStream.currentToken().value}`
			);
		}
	}

	private parseAttributeAccess() {
		this.traceStep("Parse attribute access");
		this.consumeSpecificToken(TokenKeys.IDENTIFIER);
		this.consumeSpecificToken(TokenKeys.DOT);

		// Identifier must follow dot
		if (!this._tokenStream.checkTokens([TokenKeys.IDENTIFIER])) {
			this.raiseError(`Expecting IDENTIFIER but found ${this._tokenStream.currentTokenKey()}`, DiagnosticType.SYNTAX);
		}
	}

	private parseProtected() {
		const startLine = this._tokenStream.currentToken().location.line;
		const start = this._tokenStream.currentToken().location.start;

		this.consumeSpecificToken(TokenKeys.KW_PROTECTED);
		this.consumeSpecificToken(TokenKeys.L_BRACKET);
		this.consumeInvalidWhitespace(TokenKeys.KW_OUT);
		this.consumeSpecificToken(TokenKeys.KW_OUT);
		this.consumeSpecificToken(TokenKeys.COMMA);
		this.consumeSpecificTokenStyle(TokenKeys.SPACE);

		const id = this.consumeString();

		this.consumeSpecificToken(TokenKeys.COMMA);
		this.consumeSpecificTokenStyle(TokenKeys.SPACE);
		this.consumeString();
		this.consumeInvalidWhitespace(TokenKeys.R_BRACKET);
		this.consumeSpecificToken(TokenKeys.R_BRACKET);

		const end = this._tokenStream.currentToken().location.end;

	}

	private parseOperation() {
		this.traceStep("Parse operation");
		this.consumeSpecificToken(TokenKeys.IDENTIFIER);
		this.consumeSpecificTokenStyle(TokenKeys.SPACE, "Missing space before operator");
		if (this._tokenStream.checkTokens([TokenKeys.OP_PLUS])) {
			this._tokenStream.consumeToken();
			this.consumeSpecificTokenStyle(TokenKeys.SPACE, "Missing space after operator");

			this.parseExpression();
		} else {
			this.raiseError(`Unknown token ${this._tokenStream.currentToken().name}`, DiagnosticType.SYNTAX);
		}
	}


}
