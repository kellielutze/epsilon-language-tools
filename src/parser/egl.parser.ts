import { AbstractParser, DiagnosticType } from "./abstract.parser";
import { Diagnostic, DiagnosticSeverity, Position } from "../helpers/diagnostics";
import { TokenStream } from "../tokenizer/token-stream";
import { TokenKeys } from "../tokenizer/tokens";
import { EolParser } from "./eol.parser";


export class EglParser extends AbstractParser {
	constructor(
		tokenStream: TokenStream,
	) {
		super(tokenStream);
	}

	parse(): Array<Diagnostic> {
		const nonWhiteSpaceTokenCount = this._tokenStream.getTokenNames().filter(name => name != "SPACE").length;
		if (nonWhiteSpaceTokenCount < 1) {
			this.raiseError("Empty static section", DiagnosticType.SYNTAX);
			return this._errors;
		}

		if (this._tokenStream.checkToken(TokenKeys.SPACE)) {
			this._tokenStream.consumeToken();
		} else {
			this.raiseWarning("Missing space between start of static section and section contents.", DiagnosticType.STYLE);
		}

		// Parse EOL
		let parser = new EolParser(this._tokenStream);
		parser.errors = this._errors;
		let diagnostics = parser.parse();

		if (!this._tokenStream.checkToken(TokenKeys.SPACE)) {
			this.raiseWarning(
				"Missing space between the static section contents and the end of the section.",
				DiagnosticType.STYLE
			);
		} else {
			this._tokenStream.consumeToken();
		}

		return diagnostics;
	}
}