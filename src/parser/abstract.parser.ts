import { TokenStream, Diagnostic, TokenKeys } from "..";
import { DiagnosticSeverity, Position } from "../helpers/diagnostics";
import { Token } from "../tokenizer/tokens";

export enum DiagnosticType {
	STYLE,
	SYNTAX
}

export abstract class AbstractParser {
	protected _tokenStream: TokenStream;
	protected _errors: Array<Diagnostic>;


	constructor(
		tokenStream: TokenStream,
	) {
		this._tokenStream = tokenStream;
		this._errors = [];
	}

	abstract parse(): Array<Diagnostic>;

	set errors(errors: Array<Diagnostic>) {
		this._errors = errors;
	}

	// -------------------------------------------------------------------------
	// ██████╗ ██╗ █████╗  ██████╗ ███╗   ██╗ ██████╗ ███████╗████████╗██╗ ██████╗███████╗
	// ██╔══██╗██║██╔══██╗██╔════╝ ████╗  ██║██╔═══██╗██╔════╝╚══██╔══╝██║██╔════╝██╔════╝
	// ██║  ██║██║███████║██║  ███╗██╔██╗ ██║██║   ██║███████╗   ██║   ██║██║     ███████╗
	// ██║  ██║██║██╔══██║██║   ██║██║╚██╗██║██║   ██║╚════██║   ██║   ██║██║     ╚════██║
	// ██████╔╝██║██║  ██║╚██████╔╝██║ ╚████║╚██████╔╝███████║   ██║   ██║╚██████╗███████║
	// ╚═════╝ ╚═╝╚═╝  ╚═╝ ╚═════╝ ╚═╝  ╚═══╝ ╚═════╝ ╚══════╝   ╚═╝   ╚═╝ ╚═════╝╚══════╝
	//                                                                                    
	// -------------------------------------------------------------------------

	protected raiseWarning(errorMessage: string, type: DiagnosticType) {
		this.raiseDiagnostic(errorMessage, DiagnosticSeverity.Warning, type);
	}

	protected raiseError(errorMessage: string, type: DiagnosticType) {
		this.raiseDiagnostic(errorMessage, DiagnosticSeverity.Error, type);
	}

	protected raiseDiagnostic(message: string, severity: DiagnosticSeverity, type: DiagnosticType): void {
		this.log(message);
		const currentToken = this._tokenStream.currentToken();
		this.raiseDiagnosticDetail(
			message,
			severity,
			type,
			currentToken.location.line,
			currentToken.location.start,
			currentToken.location.end,
		);
	}

	protected raiseDiagnosticDetail(
		message: string,
		severity: DiagnosticSeverity,
		type: DiagnosticType,
		line: number,
		start: number,
		end: number,
	) {
		this.log(`Number of errors ${this._errors.length + 1}`);

		const error: Diagnostic = {
			severity: severity,
			range: {
				start: Position.create(line, start),
				end: Position.create(line, end)
			},
			message: message,
			source: "language-eol",
		};


		this._errors.push(error);
	}

	protected log(message: string) {
		console.log(message);
	}

	protected logStep(message: string) {
		this.log(`PARSE -> ${message}`);
	}

	protected traceStep(message: string) {
		this.trace(`PARSE -> ${message}`);
	}

	protected trace(message: string) {
		console.log(`EglStaticSectionParser -> ${message}`);
	}

	// -----------------------------------------------------------------------------
	//  ██████╗ ██████╗ ███╗   ██╗███████╗██╗   ██╗███╗   ███╗███████╗██████╗ ███████╗
	// ██╔════╝██╔═══██╗████╗  ██║██╔════╝██║   ██║████╗ ████║██╔════╝██╔══██╗██╔════╝
	// ██║     ██║   ██║██╔██╗ ██║███████╗██║   ██║██╔████╔██║█████╗  ██████╔╝███████╗
	// ██║     ██║   ██║██║╚██╗██║╚════██║██║   ██║██║╚██╔╝██║██╔══╝  ██╔══██╗╚════██║
	// ╚██████╗╚██████╔╝██║ ╚████║███████║╚██████╔╝██║ ╚═╝ ██║███████╗██║  ██║███████║
	//  ╚═════╝ ╚═════╝ ╚═╝  ╚═══╝╚══════╝ ╚═════╝ ╚═╝     ╚═╝╚══════╝╚═╝  ╚═╝╚══════╝
	//
	// -----------------------------------------------------------------------------

	/**
	 * Return string contents and as it is consumed.
	 */
	protected consumeString(): string {
		this.consumeSpecificTokenStyleWithAlternatives(
			TokenKeys.SINGLE_QUOTE,
			`Expected single quote, found ${this._tokenStream.currentToken().value}`,
			[TokenKeys.DOUBLE_QUOTE, TokenKeys.SINGLE_QUOTE]
		);

		const expectedTerminator: TokenKeys = this._tokenStream.previousTokenKey();
		let contents: Array<Token> = [];

		while (!this._tokenStream.checkTokens([TokenKeys.EOF, expectedTerminator])) {
			contents.push(this._tokenStream.currentToken());
			this._tokenStream.consumeToken();
		}

		this.consumeSpecificToken(expectedTerminator);

		const reducer = (accumulator, currentValue) => accumulator + currentValue.value;
		return contents.reduce(reducer, contents.length);
	}

	protected consumeInvalidWhitespace(expectedToken: TokenKeys) {
		if (this._tokenStream.currentTokenKey() === TokenKeys.SPACE) {
			this.raiseWarning(`Expected ${expectedToken} but got SPACE`, DiagnosticType.STYLE);
			this.consumeWhitespace();
		}
	}

	protected consumeWhitespace() {
		if (this._tokenStream.currentTokenKey() === TokenKeys.SPACE) {
			this.traceStep("Consume whitespace");
			this._tokenStream.consumeToken();
		}
	}

	protected consumeSpecificToken(tokenKey: TokenKeys, message?: string): void {
		this.traceStep(`Consume ${tokenKey}`);
		if (!this._tokenStream.consumeSpecificToken(tokenKey)) {
			if (message) {
				this.raiseError(message, DiagnosticType.SYNTAX);
			} else {
				this.raiseError(
					`Unexpected token, expected ${tokenKey} but got ${this._tokenStream.currentToken().value}`,
					DiagnosticType.SYNTAX
				);
			}
		}
	}

	protected consumeSpecificTokenStyleWithAlternatives(
		tokenKey: TokenKeys,
		message?: string,
		validAlternatives?: Array<TokenKeys>
	): void {
		if (validAlternatives) {
			if (this._tokenStream.checkTokens(validAlternatives)) {
				this.consumeSpecificTokenStyle(tokenKey, message);
			} else {
				this.raiseError(message, DiagnosticType.SYNTAX);
			}
		} else {
			this.consumeSpecificTokenStyle(tokenKey, message);
		}
	}

	protected consumeSpecificTokenStyle(tokenKey: TokenKeys, message?: string): void {
		if (!this._tokenStream.consumeSpecificToken(tokenKey)) {
			if (message) {
				this.raiseWarning(message, DiagnosticType.STYLE);
			} else {
				this.raiseWarning(
					`Unexpected token, expected ${tokenKey} but got ${this._tokenStream.currentToken().value}`,
					DiagnosticType.STYLE
				);
			}
		}
	}

}