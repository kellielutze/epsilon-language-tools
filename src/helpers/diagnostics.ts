

export interface LintingError {
	match: RegExp;
	message: string;
	severity: DiagnosticSeverity;
}

export enum DiagnosticSeverity {
	Error = 1,
	Warning = 2,
	Information = 3,
	Hint = 4
}


export class Position {
	line: number;
	character: number;

	static create(line: number, character: number): Position {
		let position = new Position();
		position.character = character;
		position.line = line;

		return position;
	}

	toString(): string {
		return `Line ${this.line}, column ${this.character}`;
	}
}

export interface Diagnostic {
	/**
	 * The range at which the message applies
	 */
	range: Range;
	/**
	 * The diagnostic's severity. Can be omitted. If omitted it is up to the
	 * client to interpret diagnostics as error, warning, info or hint.
	 */
	severity?: DiagnosticSeverity;
	/**
	 * The diagnostic's code, which might appear in the user interface.
	 */
	code?: number | string;
	/**
	 * A human-readable string describing the source of this
	 * diagnostic, e.g. 'typescript' or 'super lint'.
	 */
	source?: string;
	/**
	 * The diagnostic's message.
	 */
	message: string;
}

export interface Range {
	start: Position;
	end: Position;
}