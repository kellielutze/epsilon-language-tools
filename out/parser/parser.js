"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const diagnostics_1 = require("../helpers/diagnostics");
const tokens_1 = require("../tokenizer/tokens");
var DiagnosticType;
(function (DiagnosticType) {
    DiagnosticType[DiagnosticType["STYLE"] = 0] = "STYLE";
    DiagnosticType[DiagnosticType["SYNTAX"] = 1] = "SYNTAX";
})(DiagnosticType = exports.DiagnosticType || (exports.DiagnosticType = {}));
/**
 * To operate on a single static section
 */
class Parser {
    constructor(tokenStream) {
        this._tokenStream = tokenStream;
    }
    parse() {
        this._errors = [];
        this.log(JSON.stringify(this._tokenStream.getTokenNames()));
        const nonWhiteSpaceTokenCount = this._tokenStream.getTokenNames().filter(name => name != "SPACE").length;
        if (nonWhiteSpaceTokenCount < 1) {
            this.raiseError("Empty static section", DiagnosticType.SYNTAX);
            return this._errors;
        }
        if (this._tokenStream.checkToken(tokens_1.TokenKeys.SPACE)) {
            this._tokenStream.consumeToken();
        }
        else {
            this.raiseWarning("Missing space between start of static section and section contents.", DiagnosticType.STYLE);
        }
        this.parseContent();
        if (!this._tokenStream.checkToken(tokens_1.TokenKeys.SPACE)) {
            this.raiseWarning("Missing space between the static section contents and the end of the section.", DiagnosticType.STYLE);
        }
        else {
            this._tokenStream.consumeToken();
        }
        return this._errors;
    }
    parseContent() {
        this.traceStep("Parse content");
        while (this._tokenStream.nextToken() != tokens_1.EOF_TOKEN) {
            if (this._tokenStream.currentTokenKey() === tokens_1.TokenKeys.SPACE) {
                this._tokenStream.consumeToken();
                this.parseContent();
            }
            else {
                this.parseExpression();
            }
        }
    }
    parseExpression() {
        this.logStep("Parse expression");
        if (this._tokenStream.checkTokens([tokens_1.TokenKeys.IDENTIFIER, tokens_1.TokenKeys.KW_PROTECTED])) {
            if (this._tokenStream.checkNextTwoTokens([tokens_1.TokenKeys.OP_PLUS])) {
                this.parseOperation();
            }
            else if (this._tokenStream.checkNextTokens([tokens_1.TokenKeys.L_BRACKET])) {
                this.parseMethod();
            }
            else {
                this.parseIdentifier();
            }
        }
        else {
            this.raiseError(`Unexpected token -> Expected <expression> but got ${this._tokenStream.currentToken().value}`, DiagnosticType.SYNTAX);
            this._tokenStream.consumeToken();
        }
    }
    parseIdentifier() {
        this.traceStep("Parse identifier");
        if (this._tokenStream.checkNextTokens([tokens_1.TokenKeys.L_BRACKET])) {
            this.parseMethod();
        }
        else if (this._tokenStream.nextTokenKey() == tokens_1.TokenKeys.DOT) {
            this.parseAttributeAccess();
        }
        else {
            this._tokenStream.consumeToken();
        }
    }
    parseMethod() {
        this.logStep("Parse method");
        if (this._tokenStream.currentTokenKey() == tokens_1.TokenKeys.KW_PROTECTED) {
            this.parseProtected();
        }
        else {
            this.consumeSpecificToken(tokens_1.TokenKeys.IDENTIFIER);
            this.consumeSpecificToken(tokens_1.TokenKeys.L_BRACKET);
            if (this._tokenStream.currentTokenKey() === tokens_1.TokenKeys.IDENTIFIER) {
                this.parseExpression();
            }
            // Handle random whitespace
            this.consumeInvalidWhitespace(tokens_1.TokenKeys.R_BRACKET);
            this.consumeSpecificToken(tokens_1.TokenKeys.R_BRACKET, `Unexpected token -> Expected ) but got ${this._tokenStream.currentToken().value}`);
        }
    }
    parseAttributeAccess() {
        this.traceStep("Parse attribute access");
        this.consumeSpecificToken(tokens_1.TokenKeys.IDENTIFIER);
        this.consumeSpecificToken(tokens_1.TokenKeys.DOT);
        // Identifier must follow dot
        if (!this._tokenStream.checkTokens([tokens_1.TokenKeys.IDENTIFIER])) {
            this.raiseError(`Expecting IDENTIFIER but found ${this._tokenStream.currentTokenKey()}`, DiagnosticType.SYNTAX);
        }
    }
    parseProtected() {
        const startLine = this._tokenStream.currentToken().location.line;
        const start = this._tokenStream.currentToken().location.start;
        this.consumeSpecificToken(tokens_1.TokenKeys.KW_PROTECTED);
        this.consumeSpecificToken(tokens_1.TokenKeys.L_BRACKET);
        this.consumeInvalidWhitespace(tokens_1.TokenKeys.KW_OUT);
        this.consumeSpecificToken(tokens_1.TokenKeys.KW_OUT);
        this.consumeSpecificToken(tokens_1.TokenKeys.COMMA);
        this.consumeSpecificTokenStyle(tokens_1.TokenKeys.SPACE);
        const id = this.consumeString();
        this.consumeSpecificToken(tokens_1.TokenKeys.COMMA);
        this.consumeSpecificTokenStyle(tokens_1.TokenKeys.SPACE);
        this.consumeString();
        this.consumeInvalidWhitespace(tokens_1.TokenKeys.R_BRACKET);
        this.consumeSpecificToken(tokens_1.TokenKeys.R_BRACKET);
        const end = this._tokenStream.currentToken().location.end;
    }
    parseOperation() {
        this.traceStep("Parse operation");
        this.consumeSpecificToken(tokens_1.TokenKeys.IDENTIFIER);
        this.consumeSpecificTokenStyle(tokens_1.TokenKeys.SPACE, "Missing space before operator");
        if (this._tokenStream.checkTokens([tokens_1.TokenKeys.OP_PLUS])) {
            this._tokenStream.consumeToken();
            this.consumeSpecificTokenStyle(tokens_1.TokenKeys.SPACE, "Missing space after operator");
            this.parseExpression();
        }
        else {
            this.raiseError(`Unknown token ${this._tokenStream.currentToken().name}`, DiagnosticType.SYNTAX);
        }
    }
    // -----------------------------------------------------------------------------
    //  ██████╗ ██████╗ ███╗   ██╗███████╗██╗   ██╗███╗   ███╗███████╗██████╗ ███████╗
    // ██╔════╝██╔═══██╗████╗  ██║██╔════╝██║   ██║████╗ ████║██╔════╝██╔══██╗██╔════╝
    // ██║     ██║   ██║██╔██╗ ██║███████╗██║   ██║██╔████╔██║█████╗  ██████╔╝███████╗
    // ██║     ██║   ██║██║╚██╗██║╚════██║██║   ██║██║╚██╔╝██║██╔══╝  ██╔══██╗╚════██║
    // ╚██████╗╚██████╔╝██║ ╚████║███████║╚██████╔╝██║ ╚═╝ ██║███████╗██║  ██║███████║
    //  ╚═════╝ ╚═════╝ ╚═╝  ╚═══╝╚══════╝ ╚═════╝ ╚═╝     ╚═╝╚══════╝╚═╝  ╚═╝╚══════╝
    //
    // -----------------------------------------------------------------------------
    /**
     * Return string contents and as it is consumed.
     */
    consumeString() {
        this.consumeSpecificTokenStyleWithAlternatives(tokens_1.TokenKeys.SINGLE_QUOTE, `Expected single quote, found ${this._tokenStream.currentToken().value}`, [tokens_1.TokenKeys.DOUBLE_QUOTE, tokens_1.TokenKeys.SINGLE_QUOTE]);
        const expectedTerminator = this._tokenStream.previousTokenKey();
        let contents = [];
        while (!this._tokenStream.checkTokens([tokens_1.TokenKeys.EOF, expectedTerminator])) {
            contents.push(this._tokenStream.currentToken());
            this._tokenStream.consumeToken();
        }
        this.consumeSpecificToken(expectedTerminator);
        const reducer = (accumulator, currentValue) => accumulator + currentValue.value;
        return contents.reduce(reducer, contents.length);
    }
    consumeInvalidWhitespace(expectedToken) {
        if (this._tokenStream.currentTokenKey() === tokens_1.TokenKeys.SPACE) {
            this.raiseWarning(`Expected ${expectedToken} but got SPACE`, DiagnosticType.STYLE);
            this.consumeWhitespace();
        }
    }
    consumeWhitespace() {
        if (this._tokenStream.currentTokenKey() === tokens_1.TokenKeys.SPACE) {
            this.traceStep("Consume whitespace");
            this._tokenStream.consumeToken();
        }
    }
    consumeSpecificToken(tokenKey, message) {
        this.traceStep(`Consume ${tokenKey}`);
        if (!this._tokenStream.consumeSpecificToken(tokenKey)) {
            if (message) {
                this.raiseError(message, DiagnosticType.SYNTAX);
            }
            else {
                this.raiseError(`Unexpected token, expected ${tokenKey} but got ${this._tokenStream.currentToken().value}`, DiagnosticType.SYNTAX);
            }
        }
    }
    consumeSpecificTokenStyleWithAlternatives(tokenKey, message, validAlternatives) {
        if (validAlternatives) {
            if (this._tokenStream.checkTokens(validAlternatives)) {
                this.consumeSpecificTokenStyle(tokenKey, message);
            }
            else {
                this.raiseError(message, DiagnosticType.SYNTAX);
            }
        }
        else {
            this.consumeSpecificTokenStyle(tokenKey, message);
        }
    }
    consumeSpecificTokenStyle(tokenKey, message) {
        if (!this._tokenStream.consumeSpecificToken(tokenKey)) {
            if (message) {
                this.raiseWarning(message, DiagnosticType.STYLE);
            }
            else {
                this.raiseWarning(`Unexpected token, expected ${tokenKey} but got ${this._tokenStream.currentToken().value}`, DiagnosticType.STYLE);
            }
        }
    }
    // ----------------------------------------------------------------------------
    // ██████╗ ██╗ █████╗  ██████╗ ███╗   ██╗ ██████╗ ███████╗████████╗██╗ ██████╗
    // ██╔══██╗██║██╔══██╗██╔════╝ ████╗  ██║██╔═══██╗██╔════╝╚══██╔══╝██║██╔════╝
    // ██║  ██║██║███████║██║  ███╗██╔██╗ ██║██║   ██║███████╗   ██║   ██║██║
    // ██║  ██║██║██╔══██║██║   ██║██║╚██╗██║██║   ██║╚════██║   ██║   ██║██║
    // ██████╔╝██║██║  ██║╚██████╔╝██║ ╚████║╚██████╔╝███████║   ██║   ██║╚██████╗
    // ╚═════╝ ╚═╝╚═╝  ╚═╝ ╚═════╝ ╚═╝  ╚═══╝ ╚═════╝ ╚══════╝   ╚═╝   ╚═╝ ╚═════╝
    //
    // ----------------------------------------------------------------------------
    raiseWarning(errorMessage, type) {
        this.raiseDiagnostic(errorMessage, diagnostics_1.DiagnosticSeverity.Warning, type);
    }
    raiseError(errorMessage, type) {
        this.raiseDiagnostic(errorMessage, diagnostics_1.DiagnosticSeverity.Error, type);
    }
    raiseDiagnostic(message, severity, type) {
        this.log(message);
        const currentToken = this._tokenStream.currentToken();
        this.raiseDiagnosticDetail(message, severity, type, currentToken.location.line, currentToken.location.start, currentToken.location.end);
    }
    raiseDiagnosticDetail(message, severity, type, line, start, end) {
        this.log(`Number of errors ${this._errors.length + 1}`);
        const error = {
            severity: severity,
            range: {
                start: diagnostics_1.Position.create(line, start),
                end: diagnostics_1.Position.create(line, end)
            },
            message: message,
            source: "language-eol",
        };
        this._errors.push(error);
    }
    log(message) {
        console.log(message);
    }
    logStep(message) {
        this.log(`PARSE -> ${message}`);
    }
    traceStep(message) {
        this.trace(`PARSE -> ${message}`);
    }
    trace(message) {
        console.log(`EglStaticSectionParser -> ${message}`);
    }
}
exports.Parser = Parser;
//# sourceMappingURL=parser.js.map