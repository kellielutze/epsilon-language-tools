"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tokens_1 = require("../tokenizer/tokens");
const abstract_parser_1 = require("./abstract.parser");
/**
 * To operate on a single static section
 */
class EolParser extends abstract_parser_1.AbstractParser {
    constructor(tokenStream) {
        super(tokenStream);
    }
    parse() {
        this.parseContent();
        return this._errors;
    }
    parseContent() {
        this.traceStep("Parse content");
        while (this._tokenStream.nextToken() != tokens_1.EOF_TOKEN) {
            if (this._tokenStream.currentTokenKey() === tokens_1.TokenKeys.SPACE) {
                this._tokenStream.consumeToken();
                this.parseContent();
            }
            else {
                this.parseExpression();
            }
        }
    }
    parseExpression() {
        this.logStep("Parse expression");
        if (this._tokenStream.checkTokens([tokens_1.TokenKeys.IDENTIFIER, tokens_1.TokenKeys.KW_PROTECTED])) {
            if (this._tokenStream.checkNextTwoTokens([tokens_1.TokenKeys.OP_PLUS])) {
                this.parseOperation();
            }
            else if (this._tokenStream.checkNextTokens([tokens_1.TokenKeys.L_BRACKET])) {
                this.parseMethod();
            }
            else {
                this.parseIdentifier();
            }
        }
        else {
            this.raiseError(`Unexpected token -> Expected <expression> but got ${this._tokenStream.currentToken().value}`, abstract_parser_1.DiagnosticType.SYNTAX);
            this._tokenStream.consumeToken();
        }
    }
    parseIdentifier() {
        this.traceStep("Parse identifier");
        if (this._tokenStream.checkNextTokens([tokens_1.TokenKeys.L_BRACKET])) {
            this.parseMethod();
        }
        else if (this._tokenStream.nextTokenKey() == tokens_1.TokenKeys.DOT) {
            this.parseAttributeAccess();
        }
        else {
            this._tokenStream.consumeToken();
        }
    }
    parseMethod() {
        this.logStep("Parse method");
        if (this._tokenStream.currentTokenKey() == tokens_1.TokenKeys.KW_PROTECTED) {
            this.parseProtected();
        }
        else {
            this.consumeSpecificToken(tokens_1.TokenKeys.IDENTIFIER);
            this.consumeSpecificToken(tokens_1.TokenKeys.L_BRACKET);
            if (this._tokenStream.currentTokenKey() === tokens_1.TokenKeys.IDENTIFIER) {
                this.parseExpression();
            }
            // Handle random whitespace
            this.consumeInvalidWhitespace(tokens_1.TokenKeys.R_BRACKET);
            this.consumeSpecificToken(tokens_1.TokenKeys.R_BRACKET, `Unexpected token -> Expected ) but got ${this._tokenStream.currentToken().value}`);
        }
    }
    parseAttributeAccess() {
        this.traceStep("Parse attribute access");
        this.consumeSpecificToken(tokens_1.TokenKeys.IDENTIFIER);
        this.consumeSpecificToken(tokens_1.TokenKeys.DOT);
        // Identifier must follow dot
        if (!this._tokenStream.checkTokens([tokens_1.TokenKeys.IDENTIFIER])) {
            this.raiseError(`Expecting IDENTIFIER but found ${this._tokenStream.currentTokenKey()}`, abstract_parser_1.DiagnosticType.SYNTAX);
        }
    }
    parseProtected() {
        const startLine = this._tokenStream.currentToken().location.line;
        const start = this._tokenStream.currentToken().location.start;
        this.consumeSpecificToken(tokens_1.TokenKeys.KW_PROTECTED);
        this.consumeSpecificToken(tokens_1.TokenKeys.L_BRACKET);
        this.consumeInvalidWhitespace(tokens_1.TokenKeys.KW_OUT);
        this.consumeSpecificToken(tokens_1.TokenKeys.KW_OUT);
        this.consumeSpecificToken(tokens_1.TokenKeys.COMMA);
        this.consumeSpecificTokenStyle(tokens_1.TokenKeys.SPACE);
        const id = this.consumeString();
        this.consumeSpecificToken(tokens_1.TokenKeys.COMMA);
        this.consumeSpecificTokenStyle(tokens_1.TokenKeys.SPACE);
        this.consumeString();
        this.consumeInvalidWhitespace(tokens_1.TokenKeys.R_BRACKET);
        this.consumeSpecificToken(tokens_1.TokenKeys.R_BRACKET);
        const end = this._tokenStream.currentToken().location.end;
    }
    parseOperation() {
        this.traceStep("Parse operation");
        this.consumeSpecificToken(tokens_1.TokenKeys.IDENTIFIER);
        this.consumeSpecificTokenStyle(tokens_1.TokenKeys.SPACE, "Missing space before operator");
        if (this._tokenStream.checkTokens([tokens_1.TokenKeys.OP_PLUS])) {
            this._tokenStream.consumeToken();
            this.consumeSpecificTokenStyle(tokens_1.TokenKeys.SPACE, "Missing space after operator");
            this.parseExpression();
        }
        else {
            this.raiseError(`Unknown token ${this._tokenStream.currentToken().name}`, abstract_parser_1.DiagnosticType.SYNTAX);
        }
    }
}
exports.EolParser = EolParser;
//# sourceMappingURL=eol.parser.js.map