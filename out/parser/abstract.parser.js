"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const __1 = require("..");
const diagnostics_1 = require("../helpers/diagnostics");
var DiagnosticType;
(function (DiagnosticType) {
    DiagnosticType[DiagnosticType["STYLE"] = 0] = "STYLE";
    DiagnosticType[DiagnosticType["SYNTAX"] = 1] = "SYNTAX";
})(DiagnosticType = exports.DiagnosticType || (exports.DiagnosticType = {}));
class AbstractParser {
    constructor(tokenStream) {
        this._tokenStream = tokenStream;
        this._errors = [];
    }
    set errors(errors) {
        this._errors = errors;
    }
    // -------------------------------------------------------------------------
    // ██████╗ ██╗ █████╗  ██████╗ ███╗   ██╗ ██████╗ ███████╗████████╗██╗ ██████╗███████╗
    // ██╔══██╗██║██╔══██╗██╔════╝ ████╗  ██║██╔═══██╗██╔════╝╚══██╔══╝██║██╔════╝██╔════╝
    // ██║  ██║██║███████║██║  ███╗██╔██╗ ██║██║   ██║███████╗   ██║   ██║██║     ███████╗
    // ██║  ██║██║██╔══██║██║   ██║██║╚██╗██║██║   ██║╚════██║   ██║   ██║██║     ╚════██║
    // ██████╔╝██║██║  ██║╚██████╔╝██║ ╚████║╚██████╔╝███████║   ██║   ██║╚██████╗███████║
    // ╚═════╝ ╚═╝╚═╝  ╚═╝ ╚═════╝ ╚═╝  ╚═══╝ ╚═════╝ ╚══════╝   ╚═╝   ╚═╝ ╚═════╝╚══════╝
    //                                                                                    
    // -------------------------------------------------------------------------
    raiseWarning(errorMessage, type) {
        this.raiseDiagnostic(errorMessage, diagnostics_1.DiagnosticSeverity.Warning, type);
    }
    raiseError(errorMessage, type) {
        this.raiseDiagnostic(errorMessage, diagnostics_1.DiagnosticSeverity.Error, type);
    }
    raiseDiagnostic(message, severity, type) {
        this.log(message);
        const currentToken = this._tokenStream.currentToken();
        this.raiseDiagnosticDetail(message, severity, type, currentToken.location.line, currentToken.location.start, currentToken.location.end);
    }
    raiseDiagnosticDetail(message, severity, type, line, start, end) {
        this.log(`Number of errors ${this._errors.length + 1}`);
        const error = {
            severity: severity,
            range: {
                start: diagnostics_1.Position.create(line, start),
                end: diagnostics_1.Position.create(line, end)
            },
            message: message,
            source: "language-eol",
        };
        this._errors.push(error);
    }
    log(message) {
        console.log(message);
    }
    logStep(message) {
        this.log(`PARSE -> ${message}`);
    }
    traceStep(message) {
        this.trace(`PARSE -> ${message}`);
    }
    trace(message) {
        console.log(`EglStaticSectionParser -> ${message}`);
    }
    // -----------------------------------------------------------------------------
    //  ██████╗ ██████╗ ███╗   ██╗███████╗██╗   ██╗███╗   ███╗███████╗██████╗ ███████╗
    // ██╔════╝██╔═══██╗████╗  ██║██╔════╝██║   ██║████╗ ████║██╔════╝██╔══██╗██╔════╝
    // ██║     ██║   ██║██╔██╗ ██║███████╗██║   ██║██╔████╔██║█████╗  ██████╔╝███████╗
    // ██║     ██║   ██║██║╚██╗██║╚════██║██║   ██║██║╚██╔╝██║██╔══╝  ██╔══██╗╚════██║
    // ╚██████╗╚██████╔╝██║ ╚████║███████║╚██████╔╝██║ ╚═╝ ██║███████╗██║  ██║███████║
    //  ╚═════╝ ╚═════╝ ╚═╝  ╚═══╝╚══════╝ ╚═════╝ ╚═╝     ╚═╝╚══════╝╚═╝  ╚═╝╚══════╝
    //
    // -----------------------------------------------------------------------------
    /**
     * Return string contents and as it is consumed.
     */
    consumeString() {
        this.consumeSpecificTokenStyleWithAlternatives(__1.TokenKeys.SINGLE_QUOTE, `Expected single quote, found ${this._tokenStream.currentToken().value}`, [__1.TokenKeys.DOUBLE_QUOTE, __1.TokenKeys.SINGLE_QUOTE]);
        const expectedTerminator = this._tokenStream.previousTokenKey();
        let contents = [];
        while (!this._tokenStream.checkTokens([__1.TokenKeys.EOF, expectedTerminator])) {
            contents.push(this._tokenStream.currentToken());
            this._tokenStream.consumeToken();
        }
        this.consumeSpecificToken(expectedTerminator);
        const reducer = (accumulator, currentValue) => accumulator + currentValue.value;
        return contents.reduce(reducer, contents.length);
    }
    consumeInvalidWhitespace(expectedToken) {
        if (this._tokenStream.currentTokenKey() === __1.TokenKeys.SPACE) {
            this.raiseWarning(`Expected ${expectedToken} but got SPACE`, DiagnosticType.STYLE);
            this.consumeWhitespace();
        }
    }
    consumeWhitespace() {
        if (this._tokenStream.currentTokenKey() === __1.TokenKeys.SPACE) {
            this.traceStep("Consume whitespace");
            this._tokenStream.consumeToken();
        }
    }
    consumeSpecificToken(tokenKey, message) {
        this.traceStep(`Consume ${tokenKey}`);
        if (!this._tokenStream.consumeSpecificToken(tokenKey)) {
            if (message) {
                this.raiseError(message, DiagnosticType.SYNTAX);
            }
            else {
                this.raiseError(`Unexpected token, expected ${tokenKey} but got ${this._tokenStream.currentToken().value}`, DiagnosticType.SYNTAX);
            }
        }
    }
    consumeSpecificTokenStyleWithAlternatives(tokenKey, message, validAlternatives) {
        if (validAlternatives) {
            if (this._tokenStream.checkTokens(validAlternatives)) {
                this.consumeSpecificTokenStyle(tokenKey, message);
            }
            else {
                this.raiseError(message, DiagnosticType.SYNTAX);
            }
        }
        else {
            this.consumeSpecificTokenStyle(tokenKey, message);
        }
    }
    consumeSpecificTokenStyle(tokenKey, message) {
        if (!this._tokenStream.consumeSpecificToken(tokenKey)) {
            if (message) {
                this.raiseWarning(message, DiagnosticType.STYLE);
            }
            else {
                this.raiseWarning(`Unexpected token, expected ${tokenKey} but got ${this._tokenStream.currentToken().value}`, DiagnosticType.STYLE);
            }
        }
    }
}
exports.AbstractParser = AbstractParser;
//# sourceMappingURL=abstract.parser.js.map