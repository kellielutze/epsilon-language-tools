"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const abstract_parser_1 = require("./abstract.parser");
const tokens_1 = require("../tokenizer/tokens");
const eol_parser_1 = require("./eol.parser");
class EglParser extends abstract_parser_1.AbstractParser {
    constructor(tokenStream) {
        super(tokenStream);
    }
    parse() {
        const nonWhiteSpaceTokenCount = this._tokenStream.getTokenNames().filter(name => name != "SPACE").length;
        if (nonWhiteSpaceTokenCount < 1) {
            this.raiseError("Empty static section", abstract_parser_1.DiagnosticType.SYNTAX);
            return this._errors;
        }
        if (this._tokenStream.checkToken(tokens_1.TokenKeys.SPACE)) {
            this._tokenStream.consumeToken();
        }
        else {
            this.raiseWarning("Missing space between start of static section and section contents.", abstract_parser_1.DiagnosticType.STYLE);
        }
        // Parse EOL
        let parser = new eol_parser_1.EolParser(this._tokenStream);
        parser.errors = this._errors;
        let diagnostics = parser.parse();
        if (!this._tokenStream.checkToken(tokens_1.TokenKeys.SPACE)) {
            this.raiseWarning("Missing space between the static section contents and the end of the section.", abstract_parser_1.DiagnosticType.STYLE);
        }
        else {
            this._tokenStream.consumeToken();
        }
        return diagnostics;
    }
}
exports.EglParser = EglParser;
//# sourceMappingURL=egl.parser.js.map