"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tokens_1 = require("./tokens");
class TokenStream {
    constructor(tokens) {
        this._tokenStream = [];
        this._numberOfTokens = 0;
        this._index = 0;
        this._tokenStream = tokens;
        this._numberOfTokens = this._tokenStream.length;
    }
    nextToken() {
        if (this._index === this._numberOfTokens - 1) {
            return tokens_1.EOF_TOKEN;
        }
        return this._tokenStream[this._index + 1];
    }
    nextNextToken() {
        if (this._index + 1 === this._numberOfTokens - 1) {
            return tokens_1.EOF_TOKEN;
        }
        return this._tokenStream[this._index + 2];
    }
    nextTokenKey() {
        return tokens_1.TokenKeys[(this.nextToken()).name];
    }
    nextNextTokenKey() {
        return tokens_1.TokenKeys[(this.nextNextToken()).name];
    }
    previousToken() {
        if (this._index === 0) {
            return tokens_1.EOF_TOKEN;
        }
        return this._tokenStream[this._index - 1];
    }
    previousTokenKey() {
        return tokens_1.TokenKeys[this.previousToken().name];
    }
    currentToken() {
        return this._tokenStream[this._index];
    }
    consumeSpecificToken(tokenKey) {
        if (this.currentTokenKey() === tokenKey) {
            this.consumeToken();
            return true;
        }
        else {
            return false;
        }
    }
    currentTokenKey() {
        return tokens_1.TokenKeys[this._tokenStream[this._index].name];
    }
    consumeToken() {
        if (this._index === this._numberOfTokens - 1) {
            return tokens_1.EOF_TOKEN;
        }
        return this._tokenStream[this._index++];
    }
    /**
     * Check if the current token matches
     *
     * @param token The token to check
     */
    checkToken(token) {
        if (this._index === this._numberOfTokens) {
            return false;
        }
        return tokens_1.TokenKeys[this._tokenStream[this._index].name] === token;
    }
    checkNextTwoTokens(tokens) {
        return tokens.includes(this.nextTokenKey())
            || tokens.includes(this.nextNextTokenKey());
    }
    checkPreviousToken(token) {
        return token === this.previousTokenKey();
    }
    checkNextTokens(tokens) {
        return tokens.includes(this.nextTokenKey());
    }
    checkTokens(tokens) {
        if (this._index === this._numberOfTokens - 1) {
            return false;
        }
        return tokens.includes(this.currentTokenKey());
    }
    getTokenNames() {
        return this._tokenStream.map(token => token.name);
    }
}
exports.TokenStream = TokenStream;
//# sourceMappingURL=token-stream.js.map