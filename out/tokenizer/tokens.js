"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TokenKeys;
(function (TokenKeys) {
    TokenKeys["SPACE"] = "SPACE";
    TokenKeys["L_BRACKET"] = "L_BRACKET";
    TokenKeys["R_BRACKET"] = "R_BRACKET";
    TokenKeys["IDENTIFIER"] = "IDENTIFIER";
    TokenKeys["OP_PLUS"] = "OP_PLUS";
    TokenKeys["NEW_LINE"] = "NEW_LINE";
    TokenKeys["TAB"] = "TAB";
    TokenKeys["EOF"] = "EOF";
    TokenKeys["KW_PROTECTED"] = "KW_PROTECTED";
    TokenKeys["KW_OUT"] = "KW_OUT";
    TokenKeys["KW_IMPORT"] = "KW_IMPORT";
    TokenKeys["KW_MAP"] = "KW_MAP";
    TokenKeys["KW_PUT"] = "KW_PUT";
    TokenKeys["KW_ELSE"] = "KW_ELSE";
    TokenKeys["KW_IF"] = "KW_IF";
    TokenKeys["KW_RETURN"] = "KW_RETURN";
    TokenKeys["KW_FOR"] = "KW_FOR";
    TokenKeys["KW_STRING"] = "KW_STRING";
    TokenKeys["KW_VAR"] = "KW_VAR";
    TokenKeys["KW_NEW"] = "KW_NEW";
    TokenKeys["L_BRACE"] = "L_BRACE";
    TokenKeys["R_BRACE"] = "R_BRACE";
    TokenKeys["KW_IN"] = "KW_IN";
    TokenKeys["KW_OPERATION"] = "KW_OPERATION";
    TokenKeys["SINGLE_QUOTE"] = "SINGLE_QUOTE";
    TokenKeys["DOUBLE_QUOTE"] = "DOUBLE_QUOTE";
    TokenKeys["COMMA"] = "COMMA";
    TokenKeys["DOT"] = "DOT";
    TokenKeys["SEMI_COLON"] = "SEMI_COLON";
    TokenKeys["B_SLASH"] = "B_SLASH";
    TokenKeys["F_SLASH"] = "F_SLASH";
    TokenKeys["STAR"] = "STAR";
    TokenKeys["DASH"] = "DASH";
    TokenKeys["COLON"] = "COLON";
    TokenKeys["EQUALS"] = "EQUALS";
    TokenKeys["AT"] = "AT";
    TokenKeys["KW_CACHED"] = "KW_CACHED";
    TokenKeys["NUMBER"] = "NUMBER";
})(TokenKeys = exports.TokenKeys || (exports.TokenKeys = {}));
exports.TOKEN_MATCHERS = {
    [TokenKeys.SPACE]: / /gm,
    [TokenKeys.L_BRACKET]: /\(/,
    [TokenKeys.R_BRACKET]: /\)/,
    [TokenKeys.R_BRACE]: /\}/,
    [TokenKeys.L_BRACE]: /\{/,
    [TokenKeys.SINGLE_QUOTE]: /\'/,
    [TokenKeys.DOUBLE_QUOTE]: /"/,
    [TokenKeys.B_SLASH]: /\\/,
    [TokenKeys.AT]: /@/,
    [TokenKeys.F_SLASH]: /\//,
    [TokenKeys.STAR]: /\*/,
    [TokenKeys.COLON]: /:/,
    [TokenKeys.DASH]: /-/,
    [TokenKeys.DOT]: /\./,
    [TokenKeys.SEMI_COLON]: /;/,
    [TokenKeys.KW_PROTECTED]: /\bprotected\b/,
    [TokenKeys.KW_NEW]: /new/,
    [TokenKeys.KW_OPERATION]: /\boperation\b/,
    [TokenKeys.KW_CACHED]: /cached/,
    [TokenKeys.KW_PUT]: /put/,
    [TokenKeys.KW_MAP]: /\bMap\b/,
    [TokenKeys.KW_ELSE]: /\belse\b/,
    [TokenKeys.KW_FOR]: /\bfor\b/,
    [TokenKeys.KW_STRING]: /\bString\b/,
    [TokenKeys.KW_IN]: /\bin\b/,
    [TokenKeys.KW_VAR]: /\bvar\b/,
    [TokenKeys.EQUALS]: /\=/,
    [TokenKeys.KW_IF]: /if/,
    [TokenKeys.KW_RETURN]: /\breturn\b/,
    [TokenKeys.KW_OUT]: /\bout\b/,
    [TokenKeys.KW_IMPORT]: /\bimport\b/,
    [TokenKeys.IDENTIFIER]: /\w+/,
    [TokenKeys.OP_PLUS]: /\+/,
    [TokenKeys.NEW_LINE]: /\n/,
    [TokenKeys.TAB]: /\t/,
    [TokenKeys.COMMA]: /,/,
    [TokenKeys.NUMBER]: /[0-9]/
};
exports.EOF_TOKEN = {
    name: "EOF",
    value: "EOF",
    location: {
        start: 0,
        end: 0,
        line: 0
    }
};
//# sourceMappingURL=tokens.js.map