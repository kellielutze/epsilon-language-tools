"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const token_stream_1 = require("./token-stream");
const tokens_1 = require("./tokens");
class Tokenizer {
    constructor() {
        this._tokenStream = [];
    }
    process(text, lineNumber = 0, startIndex = 0) {
        let currentIndex = startIndex;
        // To tokenize we split on word or whitespace boundaries.
        let rawTokens = text.split(/(\b|\s|\W)/).filter(i => i.length > 0);
        // Match tokens
        for (let i = 0; i < rawTokens.length; ++i) {
            const currentToken = rawTokens[i];
            for (const key in tokens_1.TOKEN_MATCHERS) {
                if (currentToken.match(tokens_1.TOKEN_MATCHERS[key])) {
                    this._tokenStream.push({
                        name: key,
                        value: currentToken,
                        location: {
                            start: currentIndex,
                            end: currentIndex + currentToken.length,
                            line: lineNumber
                        }
                    });
                    break;
                }
            }
            currentIndex += currentToken.length;
        }
        return new token_stream_1.TokenStream(this._tokenStream);
    }
}
exports.Tokenizer = Tokenizer;
//# sourceMappingURL=tokenizer.js.map