"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DiagnosticSeverity;
(function (DiagnosticSeverity) {
    DiagnosticSeverity[DiagnosticSeverity["Error"] = 1] = "Error";
    DiagnosticSeverity[DiagnosticSeverity["Warning"] = 2] = "Warning";
    DiagnosticSeverity[DiagnosticSeverity["Information"] = 3] = "Information";
    DiagnosticSeverity[DiagnosticSeverity["Hint"] = 4] = "Hint";
})(DiagnosticSeverity = exports.DiagnosticSeverity || (exports.DiagnosticSeverity = {}));
class Position {
    static create(line, character) {
        let position = new Position();
        position.character = character;
        position.line = line;
        return position;
    }
    toString() {
        return `Line ${this.line}, column ${this.character}`;
    }
}
exports.Position = Position;
//# sourceMappingURL=diagnostics.js.map